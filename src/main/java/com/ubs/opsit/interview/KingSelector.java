package com.ubs.opsit.interview;

import java.util.ArrayList;
import java.util.List;

public class KingSelector {
	
	public KingCandidate selectKingSuccessor (List<KingCandidate> dynasty){
		List<KingCandidate> maleCandidates = new ArrayList<KingCandidate>();
		List<KingCandidate> femaleCandidates = new ArrayList<KingCandidate>();
		for (KingCandidate candidate : dynasty){
			if (candidate.male){
				maleCandidates.add(candidate);
			}
			else{
				femaleCandidates.add(candidate);
			}
			
		}
		if(maleCandidates.isEmpty()){
			return checkOldest(femaleCandidates);
		}
		else return checkOldest(maleCandidates);
	}
	
	public KingCandidate checkOldest(List<KingCandidate> dynasty){
		KingCandidate oldest = new KingCandidate();
		for (KingCandidate candidate : dynasty){
			if(candidate.age > oldest.age){
				oldest = candidate;
			}
		}
		return oldest;
	}

}
