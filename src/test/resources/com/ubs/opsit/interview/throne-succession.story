Narrative:
    As a king's clerk
    I want to have a tool to ease my work in terms defining throne successor
    So that I won't be beheaded for wrong decision

Story:  True king successor selector feature

Scenario: King's offspring composed by male and female 
Given I have a King's offspring composed by male and female candidates
When I call the KingSelector class
Then I should see that the true king successor is the oldest male candidate


Scenario: King's offspring composed by female only 
Given I have a King's offspring composed only by female candidates
When I call the KingSelector class
Then I should see that the true king successor is the oldest female candidate