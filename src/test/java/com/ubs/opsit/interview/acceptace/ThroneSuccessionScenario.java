package com.ubs.opsit.interview.acceptace;

import static com.ubs.opsit.interview.acceptace.framework.BehaviourTestEmbedder.aBehaviouralTestRunner;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Test;

import com.ubs.opsit.interview.KingCandidate;
import com.ubs.opsit.interview.KingSelector;

/**
 * Acceptance test class that uses the JBehave (Gherkin) syntax for writing stories.  You need to
 * edit this class to complete the exercise.
 */
public class ThroneSuccessionScenario {
    @Test
    public void throneSuccessionAcceptanceTests() throws Exception {
        aBehaviouralTestRunner()
                .usingStepsFrom(this)
                .withStory("throne-succession.story")
                .run();
    }
    
    List<KingCandidate> dynasty =  new ArrayList<KingCandidate>();
    KingCandidate trueKingSuccessor = new KingCandidate();
    KingSelector caller = new KingSelector();
    
    //@Test
    @Given("I have a King's offspring composed by male and female candidates")
    //@Pending
    public void givenIHaveAKingsOffspringComposedByMaleAndFemaleCandidates() {
    	dynasty =  objectBuilderMaleAndFemale();
    }

    //@Test
    @When("I call the KingSelector class")
    //@Pending
    public void whenICallTheKingSelectorClass() {
    	trueKingSuccessor = caller.selectKingSuccessor(dynasty);
    }

    //@Test
    @Then("I should see that the true king successor is the oldest male candidate")
    //@Pending
    public void thenIShouldSeeThatTheTrueKingSuccessorIsTheOldestMaleCandidate() {
    	assertEquals(true, trueKingSuccessor.male);
    	assertEquals("Jon", trueKingSuccessor.name);
    }

    //@Test
    @Given("I have a King's offspring composed only by female candidates")
    //@Pending
    public void givenIHaveAKingsOffspringComposedOnlyByFemaleCandidates() {
    	dynasty =  objectBuilderFemaleOnly();
    }

    //@Test
    @Then("I should see that the true king successor is the oldest female candidate")
    //@Pending
    public void thenIShouldSeeThatTheTrueKingSuccessorIsTheOldestFemaleCandidate() {
    	assertEquals(false, trueKingSuccessor.male);
    	assertEquals("Liana", trueKingSuccessor.name);
    }
    
     
//    @Test
//    public void checkKingSuccessorMaleAndFemale(){
//    	
//    	List<KingCandidate> dynasty =  objectBuilderMaleAndFemale();
//    	KingCandidate trueKingSuccessor = new KingCandidate();
//    	
//		KingSelector caller = new KingSelector();
//		trueKingSuccessor = caller.selectKingSuccessor(dynasty);
//		assertEquals("Jon", trueKingSuccessor.name);
//	}
//    
//    @Test
//    public void checkKingSuccessorFemaleOnly(){
//    	
//    	List<KingCandidate> dynasty =  objectBuilderFemaleOnly();
//    	KingCandidate trueKingSuccessor = new KingCandidate();
//    	
//		KingSelector caller = new KingSelector();
//		trueKingSuccessor = caller.selectKingSuccessor(dynasty);
//		assertEquals("Liana", trueKingSuccessor.name);
//	}
//    
    public List<KingCandidate> objectBuilderMaleAndFemale(){
    	List<KingCandidate> dynasty = new ArrayList<KingCandidate>();
    	    	
		KingCandidate candidate1 = new KingCandidate();
		candidate1.name = "Rob";
		candidate1.age = 21;
		candidate1.male = true;
		candidate1.situation = "Test";
		
		KingCandidate candidate2 = new KingCandidate();
		candidate2.name = "Sansa";
		candidate2.age = 30;
		candidate2.male = false;
		candidate2.situation = "Test";
		
		KingCandidate candidate3 = new KingCandidate();
		candidate3.name = "Jon";
		candidate3.age = 40;
		candidate3.male = true;
		candidate3.situation = "Test";
		
		dynasty.add(candidate1);
		dynasty.add(candidate2);
		dynasty.add(candidate3);
		return dynasty;
    }
    
    public List<KingCandidate> objectBuilderFemaleOnly(){
    	List<KingCandidate> dynasty = new ArrayList<KingCandidate>();
    	    	
		KingCandidate candidate1 = new KingCandidate();
		candidate1.name = "Arya";
		candidate1.age = 19;
		candidate1.male = false;
		candidate1.situation = "Test";
		
		KingCandidate candidate2 = new KingCandidate();
		candidate2.name = "Sansa";
		candidate2.age = 30;
		candidate2.male = false;
		candidate2.situation = "Test";
		
		KingCandidate candidate3 = new KingCandidate();
		candidate3.name = "Liana";
		candidate3.age = 45;
		candidate3.male = false;
		candidate3.situation = "Test";
		
		dynasty.add(candidate1);
		dynasty.add(candidate2);
		dynasty.add(candidate3);
		return dynasty;
    }
}
